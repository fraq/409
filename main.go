package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	prompt "github.com/c-bata/go-prompt"
	"github.com/slack-go/slack"
)

type SlackContext struct {
	team          *slack.TeamInfo
	current       slack.Conversation
	api           *slack.Client
	conversations map[string]slack.Conversation
}

var ctx *SlackContext

const USAGE = `
409 -- It does the scrubbing so you don't have to!

Usage: 409 [slack token]

409 is a CLI tool that allows you to interact with your Slack team and delete
messages in bulk.

Navigation:
open	-- Opens a channel, IM, or multi-party IM for interaction
close 	-- Closes a channel, IM, or multi-party IM
help	-- Prints a list of available commands
scrub 	-- Delete all your message from the currently selected channel
exit	-- Exit the program

---
Understanding the command prompt:
Your command prompt is a helpful indicator of your current status and context.

When you first launch 409, you will see a prompt resembling the following:
superSecreSlackTeam/(no channel)>

This indicates the name of your slack team and the fact that you have no
currently selected conversation. The > character indicates that you are not in
a context that allows for you to take any action with your slack team.


To interact with slack itself, open a channel or conversation:
superSecretSlackTeam/(no channel)> open fraq
superSecretSlackTeam/fraq (IM)$

This indicates you are interacting with the "fraq" conversation, along with
extra information letting you know this is an IM, or private 1:1 conversation.

To stop interacting with this conversation, close it.
superSecretSlackTeam/fraq (IM)$ close
superSecretSlackTeam/(no channel)>

You can also select a different channel without having to close the previous
one.
superSecretSlackTeam/fraq (IM)$ open foo
superSecretSlackTeam/foo $ close

Note that this one does not indicate it is an IM. This means it is either a
public or private channel.

---
Listing conversations:
Any IM or private channel you are a member of will be listed and cached when the
program starts, as well as all public channels (including archived public
channels).

Channel names are suggested in autocomplete to help with finding and spelling
them. If you are looking for a group message that is not in a channel, try
starting your search with "mpdm-" or "mpim-" (for multi-party direct/instant
message)
`

const HELP = `
HINT: You can use tab to autocomplete a suggestion

open	-- Opens a channel, IM, or multi-party IM for interaction
close 	-- Closes a channel, IM, or multi-party IM
help	-- Prints a list of available commands
scrub 	-- Delete all your message from the currently selected channel
exit	-- Exit the program
`

func main() {
	if len(os.Args) == 1 {
		fmt.Println("Error: not enough arguments.\nThis program requires a slack token to operate.")
		fmt.Println(USAGE)
		os.Exit(1)
	}

	fmt.Println("Getting a list of all your channels and IMs. This might take a second...")
	api := slack.New(os.Args[1])

	teamInfo, err := api.GetTeamInfo()
	if err != nil {
		log.Fatal(err)
	}

	ctx = &SlackContext{
		team:          teamInfo,
		current:       slack.Conversation{},
		api:           api,
		conversations: make(map[string]slack.Conversation),
	}

	conversations := getAllConversations(api)
	// User IDs (not names) are included in the returned Conversation struct
	// Perform and cache a conversion to enable user-friendly lookup
	for _, v := range conversations {
		if v.NameNormalized == "" {
			u, _ := api.GetUserInfo(v.User)
			ctx.conversations[u.Name] = v
			suggestions = append(suggestions, prompt.Suggest{u.Name, "Direct message"})
		} else {
			ctx.conversations[v.NameNormalized] = v
			suggestions = append(suggestions, prompt.Suggest{v.NameNormalized, "Channel or MPDM"})
		}
	}

	fmt.Println("Done! Happy scrubbing :)")

	p := prompt.New(
		executor,
		completer,
		prompt.OptionPrefix(teamInfo.Name+"> (no channel)"),
		prompt.OptionLivePrefix(livePrefix),
		prompt.OptionTitle("409: Scrubbing Bubbles for Slack"),
	)
	p.Run()
}

func completer(in prompt.Document) []prompt.Suggest {
	w := in.GetWordBeforeCursor()
	if w == "" {
		return []prompt.Suggest{}
	}
	return prompt.FilterHasPrefix(suggestions, w, true)
}

var suggestions = []prompt.Suggest{
	{"open", "Open a channel, IM, or group IM"},
	{"exit", "Exit 409 Slack Cleaner"},
	{"close", "Close a channel, IM, or group IM"},
	{"scrub", "Scrub all of your messages in the currently selected channel"},
	{"list", "List all your conversations and channels"},
	{"help", "Get help"},
}

func livePrefix() (string, bool) {
	if ctx.current.NameNormalized == "" && ctx.current.IsIM == false {
		return ctx.team.Name + "/(no channel)> ", false
	}

	if ctx.current.IsIM {
		u, _ := ctx.api.GetUserInfo(ctx.current.User)
		return ctx.team.Name + "/" + u.Name + " (IM)$ ", true
	}

	return ctx.team.Name + "/" + ctx.current.NameNormalized + "$ ", true
}

func executor(in string) {
	in = strings.TrimSpace(in)

	blocks := strings.Split(in, " ")
	switch blocks[0] {
	case "exit":
		fmt.Println("Bye!")
		os.Exit(0)
	case "help":
		fmt.Println(HELP)
	case "?":
		fmt.Println(HELP)
	case "open":
		if len(blocks) > 1 {
			ctx.current = ctx.conversations[blocks[1]]
		}
	case "close":
		ctx.current = slack.Conversation{}

	case "scrub":
		scrubChannel(ctx.api, ctx.current)
	case "list":
		for _, v := range ctx.conversations {
			fmt.Println(v.NameNormalized)
		}
	}
}
