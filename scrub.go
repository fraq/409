package main

import (
	"fmt"
	"time"

	"github.com/slack-go/slack"
)

func scrubChannel(api *slack.Client, conv slack.Conversation) {
	var messages []slack.Message
	params := &slack.GetConversationHistoryParameters{
		ChannelID: conv.ID,
		Cursor:    "",
	}

	for {
		resp, err := api.GetConversationHistory(params)
		if err != nil {
			fmt.Println("Unable to retrieve messages: " + err.Error())
			break
		}

		replies, err := batchGetAllReplies(api, resp.Messages)
		if err != nil {
			fmt.Println("Unable to get replies for this batch of messages")
		}

		messages = append(messages, resp.Messages...)
		messages = append(messages, replies...)

		if !(resp.HasMore) {
			break
		}
		params.Cursor = resp.ResponseMetaData.NextCursor
	}

	fmt.Printf("Found %d messages\n", len(messages))
	fmt.Println(`
Deleting your messages in this channel.
Warning: You may see errors as we attempt to delete messages that aren't ours.
***`)
	for _, v := range messages {
		_, _, err := api.DeleteMessage(conv.ID, v.Msg.Timestamp)
		if err != nil {
			fmt.Println(err)
		}
		// Sleep to match Slack's tier 3 rate limit
		time.Sleep(900 * time.Millisecond)
	}
	return
}

func batchGetAllReplies(api *slack.Client, m []slack.Message) ([]slack.Message, error) {
	// You'd think that slack.GetAllConversationReplies would use parameters accessible in slack.Conversation but noooooooo
	var (
		replies []slack.Message
		cursor  string
	)

	fmt.Printf("Getting replies for %d messages...\n", len(m))
	for _, v := range m {
		repliesParams := &slack.GetConversationRepliesParameters{
			ChannelID: ctx.current.ID,
			Timestamp: v.Msg.Timestamp,
			Cursor:    cursor,
			Inclusive: true,
		}

		fmt.Printf("Getting replies to message at timestamp %s in channel %s\n", v.Msg.Timestamp, ctx.current.ID)
		msgs, _, _, err := api.GetConversationReplies(repliesParams)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Printf("Found %d messages\n", len(replies))
		replies = append(replies, msgs...)
	}

	return replies, nil
}
