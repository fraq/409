package main

import (
	//	"fmt"
	"log"

	"github.com/slack-go/slack"
)

func getAllConversations(api *slack.Client) []slack.Conversation {
	var allConversations []slack.Conversation

	params := &slack.GetConversationsParameters{
		Types: []string{"public_channel,private_channel,mpim"},
	}

	for {
		c, cursor, err := api.GetConversations(params)
		if err != nil {
			log.Println("Unable to retrieve list of conversations: " + err.Error())
			return allConversations
		}

		for _, channel := range c {
			allConversations = append(allConversations, channel.GroupConversation.Conversation)
		}

		if cursor == "" {
			break
		}
		params.Cursor = cursor
	}

	ims, err := api.GetIMChannels()
	if err != nil {
		log.Println("Unable to retrieve list of IMs: " + err.Error())
		return allConversations
	}

	for _, v := range ims {
		allConversations = append(allConversations, v.Conversation)
	}

	return allConversations
}
