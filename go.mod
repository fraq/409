module gitlab.com/fraq/409

go 1.12

require (
	github.com/c-bata/go-prompt v0.2.3
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942 // indirect
	github.com/slack-go/slack v0.6.3
	github.com/spf13/cobra v0.0.7
	github.com/spf13/viper v1.6.2
)
