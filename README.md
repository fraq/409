# 409

```
409 -- It does the scrubbing so you don't have to!

Usage: 409 [slack token]

409 is a CLI tool that allows you to interact with your Slack team and delete
messages in bulk.

Navigation:
open	-- Opens a channel, IM, or multi-party IM for interaction
close 	-- Closes a channel, IM, or multi-party IM
help	-- Prints a list of available commands
scrub 	-- Delete all your message from the currently selected channel
exit	-- Exit the program

---
Understanding the command prompt:
Your command prompt is a helpful indicator of your current status and context.

When you first launch 409, you will see a prompt resembling the following:
superSecreSlackTeam/(no channel)>

This indicates the name of your slack team and the fact that you have no
currently selected conversation. The > character indicates that you are not in
a context that allows for you to take any action with your slack team.


To interact with slack itself, open a channel or conversation:
superSecretSlackTeam/(no channel)> open fraq
superSecretSlackTeam/fraq (IM)$

This indicates you are interacting with the "fraq" conversation, along with
extra information letting you know this is an IM, or private 1:1 conversation.

To stop interacting with this conversation, close it.
superSecretSlackTeam/fraq (IM)$ close
superSecretSlackTeam/(no channel)>

You can also select a different channel without having to close the previous
one.
superSecretSlackTeam/fraq (IM)$ open foo
superSecretSlackTeam/foo $ close

Note that this one does not indicate it is an IM. This means it is either a
public or private channel.

---
Listing conversations:
Any IM or private channel you are a member of will be listed and cached when the
program starts, as well as all public channels (including archived public
channels).

Channel names are suggested in autocomplete to help with finding and spelling
them. If you are looking for a group message that is not in a channel, try
starting your search with "mpdm-" or "mpim-" (for multi-party direct/instant
message)
```
